Prepare Blog
===================
### Adresse du site
[git lab](https://gitlab.com/jingl3s/scripts)



### Ce script réalise les traitements suivants :
* Liste toutes les images (jpg) présentent dans le dossier paramétré
* Redimensionne les images (jpg) pour être inférieur À 2Mo (limite serveur blog free.fr),
  *  Conserve l'information exif de rotation
  * Ajoute l'information pour avoir du JPG progressif
* S'identifie sur l'adresse du blog dotclear de free.fr
* Transmet les images dans le dossier media en utilisant le formulaire et image par image dans l'emplacement définie dans les paramètres
* Rédige un nouveau billet avec :
  * le titre des paramètres ainsi que la date et l'heure,
  * La catégorie choisie
  * Le billet mis à non publié
  * Le contenu du message avec l'affichage de toutes les images

### Paramétrage
Au premier lancement, le script va créé un fichier PrepareBlog.ini dans le dossier du script Python où il faut définir les éléments utilisés.

>**Exemple de contenu de fichier utilisé**

>[PrepBlog]
>blog_login = utilisateur

>blog_pass = mot de passe

>blog_post_category = Canada_2014

>blog_post_title = Vie a Montreal

>blog_url_admin = acces section administration du blog dotclear

>blog_url_media_folder_used = Quebec_2014/Montreal2

>blog_url_media_for_link = public/

>src_folder = dossier avec le path complet contenant les images a envoyer


### UML diagrams
Regarder le contenu du dossier diagrammes

### Dépendances
Paquetage Python
* [Mechanize](https://pypi.python.org/pypi/mechanize/)
* PIL (manipulation d'images)



### Liens
* Mechanize
Bibliothèque Python pour communiquer avec un site Web et permet de sauvegarder les cookies de connexion.
  * [http://wwwsearch.sourceforge.net/mechanize/](http://wwwsearch.sourceforge.net/mechanize/)

  * [Gestion des éléments d'une page Web](http://wwwsearch.sourceforge.net/mechanize/forms.html)

  * [Utilisation mechanize](http://stockrt.github.io/p/emulating-a-browser-in-python-with-mechanize/)

  * [Avec BeatifulSoup pour aller plus loin](http://www.ibm.com/developerworks/library/l-python-mechanize-beautiful-soup/)
  * [Avec BeatifulSoup suite](http://palewi.re/posts/2008/04/20/python-recipe-grab-a-page-scrape-a-table-download-a-file/)
  
  * [mechanize cheat](http://www.pythonforbeginners.com/cheatsheet/python-mechanize-cheat-sheet)
  * [mechanize usage](http://www.pythonforbeginners.com/mechanize/browsing-in-python-with-mechanize)

* Python et interaction Web pour plus tard

  * [urllib](http://www.codercaste.com/2009/11/28/how-to-use-the-urllib-python-library-to-fetch-url-data-and-more/)

  * [urllib2](http://pymotw.com/2/urllib2/)

  * [urllib3 en cours de dev](http://urllib3.readthedocs.org/en/latest/helpers.html#urllib3.filepost.encode_multipart_formdata)

  * [urllib et httplib connexion](http://www.emoticode.net/python/http-post-request-with-urllib-and-httplib.html)

* Codage des fichiers Python

  * [PEP-0263](http://legacy.python.org/dev/peps/pep-0263/)

* Python divers

  * [List vers String](http://stackoverflow.com/questions/3292643/python-convert-list-of-tuples-to-string)

  * [Gestion du temps](https://docs.python.org/2.7/library/time.html?highlight=gmtime#time.gmtime)

  * [Plein d'astuces](http://www.blog.pythonlibrary.org/)

  * [Expressions régulieres](https://developers.google.com/edu/python/regular-expressions)

* Conversion image

  * [ImageMagick JPG progressif](http://www.imagemagick.org/discourse-server/viewtopic.php?t=22141)

  * [PIL et Exif](http://pythonpath.wordpress.com/2013/04/01/working-with-pil-and-exif/)

* Autres methodes possibles pour communication avec DotClear

  * [XMLRPC](http://www.bortzmeyer.org/xml-rpc-for-blogs.html)
  
  * [PHP XMLRPC](https://github.com/Spir/Dotclear)
  
  
  
  https://docs.python.org/2/library/webbrowser.html

### Markdown editor

[https://stackedit.io/editor](https://stackedit.io/editor)
