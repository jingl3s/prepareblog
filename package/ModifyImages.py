'''
Created on 14 nov. 2014

@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''

import os
from PIL import Image
import logging

class ModifyImages(object):
    '''
    classdocs
    '''

    def __init__(self, p_root_folder, mode_pil = True):
        '''
        Constructor
        '''
        self._root_folder = p_root_folder
        self._convert_mode_pil = mode_pil
        logging.basicConfig()
        self._logger = logging.getLogger(__file__)
        
        if mode_pil == True:
            self._logger.info("Mode transformation image : " + "PIL")
        else:
            self._logger.info("Mode transformation image : " + "ImageMagick")

    #===========================================================================
    # 
    #===========================================================================
    def _get_exif(self,fn):
        from PIL.ExifTags import TAGS
        
        ret = {}
        i = Image.open(fn)
        info = i.tag.tags
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            ret[decoded] = value
        return ret
    
    
    #===============================================================================
    # Resize the request image file using internal Python Image library
    #===============================================================================
    def _resize_with_PIL(self,image_name):
        im1 = Image.open(image_name)
    
        # Resize image with a radio
        ratio = 0.9
        out = im1.resize( [int(ratio * s) for s in im1.size] )

        # Conserve image jpeg orientation reported from source to new
        if hasattr(im1, '_getexif'):
            orientation = 0x0112
            exif = im1._getexif()
            if exif is not None:
                orientation = exif[orientation]
                rotations = {
                    3: Image.ROTATE_180,
                    6: Image.ROTATE_270,
                    8: Image.ROTATE_90
                }
                if rotations.has_key(orientation):
                    out = im1.transpose(rotations[orientation])
        
#         out.save(self._root_folder + os.path.sep + image_name, 'JPEG', quality=88)
#         out.save(self._root_folder + os.path.sep + image_name, 'JPEG', quality=88, optimize=True, progressive=True)
#         out.save(self._root_folder + os.path.sep + image_name, 'JPEG', quality=88, optimize=False, progressive=True)
        # quality not set to avoid changing compression
        # optimize : If the image is reduced
        # progressive : optimise image loading on web page
        out.save(self._root_folder + os.path.sep + image_name, 'JPEG', optimize=False, progressive=True)
    
    #===============================================================================
    # Resize the request image file using internal Python Image library
    #===============================================================================
    def _resize_with_PIL(self,image_name):
        im1 = Image.open(image_name)
    
        # Resize image with a radio
        ratio = 0.9
        out = im1.resize( [int(ratio * s) for s in im1.size] )

        # Conserve image jpeg orientation reported from source to new
        if hasattr(im1, '_getexif'):
            orientation = 0x0112
            exif = im1._getexif()
            if exif is not None:
                orientation = exif[orientation]
                rotations = {
                    3: Image.ROTATE_180,
                    6: Image.ROTATE_270,
                    8: Image.ROTATE_90
                }
                if rotations.has_key(orientation):
                    out = im1.transpose(rotations[orientation])
        
#         out.save(self._root_folder + os.path.sep + image_name, 'JPEG', quality=88)
#         out.save(self._root_folder + os.path.sep + image_name, 'JPEG', quality=88, optimize=True, progressive=True)
#         out.save(self._root_folder + os.path.sep + image_name, 'JPEG', quality=88, optimize=False, progressive=True)
        # quality not set to avoid changing compression
        # optimize : If the image is reduced
        # progressive : optimise image loading on web page
        out.save(self._root_folder + os.path.sep + image_name, 'JPEG', optimize=False, progressive=True)
    
    #===============================================================================
    # Resize the request image file using external program convert from ImageMagick
    #===============================================================================
    def _resize_with_ImageMagic(self,image_name):
        # Request Image Magick to resize the image to 80% and use auto orientation   
        os.system("convert " + self._root_folder + os.path.sep\
                  + image_name + " -filter lanczos2 -resize 80% -auto-orient " + self._root_folder + os.path.sep\
                  + image_name)
    
    #===============================================================================
    # Resize the image in the defined folder
    #===============================================================================
    def resize_image(self,image_name):
        
        os.chdir(self._root_folder)
        
        while (os.path.getsize(image_name)/1024) > 2048:
            self._logger.info("Redimensionnement : " + image_name)
            if self._convert_mode_pil == True:
                self._resize_with_PIL(image_name)
            else:
                self._resize_with_ImageMagic(image_name)

    #===============================================================================
    # Resize the request image by creating a copy
    #===============================================================================
    def _resize_with_ImageMagic_medium(self,image_name, dest_image_name):
        # Request Image Magick to resize the image to 80% and use auto orientation   
        os.system("convert " + self._root_folder + os.path.sep\
                  + image_name + " -filter lanczos2 -resize 680x \
          -unsharp 0x1 -auto-orient " + self._root_folder + os.path.sep\
                  + dest_image_name)

    def copy_resize_image_medium(self,image_name, dest_image_name):
        '''
        Copy the image and resize the in the defined folder  
        :param image_name:
        '''
        
        os.chdir(self._root_folder)
        
        self._logger.info("Redimensionnement avec copy de : %s, vers : %s" % ( image_name, dest_image_name))
        if self._convert_mode_pil == True:
            raise RuntimeError("Process does not exists")
        else:
            self._resize_with_ImageMagic_medium(image_name, dest_image_name)
                
# EOF