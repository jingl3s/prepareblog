#-*-coding:utf8;-*-
'''
File used to interact with website

@author: jingl3s

'''

# license
#
# Producer 2019 jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import logging
import os
from requests import session

from bs4 import BeautifulSoup
import re


class InterractionBlog():
    '''
    InterractionBlog: Classe pour accéder au blog dotclear de free.fr
    '''
    PROP_TAG_USER = "user_id"
    PROP_TAG_PWD = "user_pwd"
    PROP_TAG_FILE_TITLE = "upfiletitle"
    PROP_TAG_FILE_UP = "upfile"
    PROP_TAG_POST_TITLE = "post_title"
    PROP_TAG_POST_CONTENT = "post_content"
    PROP_TAG_POST_STATUS = "post_status"
    PROP_TAG_POST_CATEGORY = "cat_id"
    LOGIN_SUCCESS_TEXT = "Tableau de bord"

    def __init__(self, p_blog_address, p_media_folder, p_blog_category, p_test_mode=False):
        '''
        Constructor
        '''
        self._logger = logging.getLogger(self.__class__.__name__)
        self._form = None
        self._blog_address = p_blog_address

        self._login_address = self._blog_address\
            + "/auth.php"

        self._media_folder = p_media_folder

        self.PROP_URL_MEDIA = self._blog_address\
            + "/media.php?popup=0&post_id=&d="\
            + p_media_folder
        self.PROP_URL_POST = self._blog_address\
            + "/post.php"

        self._blog_post_category = p_blog_category

        self._test_mode = p_test_mode

        self._requests_session = session()

    def login_blog(self, p_login, p_pass):
        '''
        Login to the blog with address given display the login page
        :param p_login:
        :param p_pass:
        '''
        login_success = False

        self._logger.info("Connexion en cours")

        try:

            # Envoi informations de connexion directement sur la page de
            # connexion
            payload = {'user_id': p_login,
                       'user_pwd': p_pass
                       }
            response = self._requests_session.post(
                self._login_address, data=payload, verify=False)

            # Verification si le login est un succes
            if self.LOGIN_SUCCESS_TEXT in response.text:
                self._logger.info("Login succes")
                login_success = True
            else:
                self._logger.error("Authentification erreur")

            # Request display of index page in order to store the cookie
            self._requests_session.get(self._blog_address + "/index.php")

        except Exception:
            self._logger.exception("Error de connexion au serveur.")
            raise
        return login_success

    def upload_photo(self, filename):
        '''
        Send the Picture to the blog
        Upload a photo to the blog after a successfull login

        :param filename: the filename to upload
        '''

        filename_only = os.path.basename(filename)
        self._logger.info("Blog uploading: " + filename_only)

        if not self._test_mode:

            # Affichage de la page de media pour avoir les jetons a la bonne
            # place
            self._requests_session.get(self.PROP_URL_MEDIA)

            # Ajout envoi du fichier
            # Source:
            # https://stackoverflow.com/questions/46178327/python-requests-unable-to-upload-image-to-multipart-form-data
            # https://stackoverflow.com/questions/29104107/upload-image-using-post-form-data-in-python-requests
            # https://stackoverflow.com/questions/22567306/python-requests-file-upload
            with open(filename, 'rb') as img:
                data_file = {
                    'upfile': (filename_only, img, 'image/jpeg'),
                    'xd_check': 'bf323fffb8c8907c07e7dfe6c498070346a32431',
                    'MAX_FILE_SIZE': '2097152',
                    'd': self._media_folder,
                    'upfiletitle': filename_only
                }
                
                # TODO: Check if valid
#                 payload = {
#                     'upfiletitle': filename_only
#                     }

                r = self._requests_session.post(
                    self._blog_address + '/media.php?popup=0&post_id=', files=data_file)
#                 self._blog_address + '/media.php?popup=0&post_id=', files=data_file, data=payload)

                self._logger.debug(
                    "Status code apres envoi image: " + str(r.status_code))

            response = self._requests_session.get(
                self._blog_address + '/media.php?popup=0&post_id=&d=Canada_2019%2FToronto%2F&upok=1')
            output_text = response.text

        if not ("Fichier charg".lower() in output_text.lower() and "avec succ".lower() in output_text.lower()):
            self._logger.error(
                "Blog image load fails: " + filename_only + ".\nDonnée recu : " + str(output_text))

    def create_new_ticket(self, p_tuple_text, picture_name_to_check, p_post_title):
        '''
        Create a new ticket on the blog

        @param 
        filename: the filename to upload

        '''

        if self._test_mode:
            return ""

        response_get = self._requests_session.get(self.PROP_URL_POST)

        soup = BeautifulSoup(response_get.text, "html.parser")
        select_category = soup.find(
            "select", {"id": self.PROP_TAG_POST_CATEGORY})

        category_id = ''
        select_options = select_category.find_all('option')
        for sel_option in select_options:
            if sel_option.text.endswith(self._blog_post_category):
                category_id = sel_option.attrs['value']

        if category_id == '':
            raise ValueError("Category non definie pour le billet: {}".format(
                self._blog_post_category))

        xd_check_value = soup.find("input", {"name": 'xd_check'})

        content = '\n'.join(map(str, p_tuple_text))
        #content = '\n'.join(p_tuple_text)
        payload = {
            'cat_id': category_id,
            self.PROP_TAG_POST_STATUS: '0',
            'post_dt': '',
            'post_format': 'wiki',
            'post_open_comment': '1',
            'post_lang': 'fr',
            'post_password': '',
            'post_tags': '',
            self.PROP_TAG_POST_TITLE: p_post_title,
            'post_excerpt': '',
            self.PROP_TAG_POST_CONTENT: content,
            'post_notes': '',
            'save': 'enregistrer+(s)',
            'xd_check': xd_check_value.attrs['value']
        }

        r = self._requests_session.post(
            self.PROP_URL_POST, data=payload)
        self._logger.debug(
            "Status code apres POST message billet: " + str(r.status_code))

        # Read the return to check if the picture is correctly uploaded
        data = r.url

        pattern = r"post\.php\?id=(\d+)&"

        addresse = ""
        #output_text = r.text
        # if string.find(data, picture_name_to_check) == -1:
        #    self._logger.error("Blog create new ticket fails.")
        # else:
        #    self._logger.info("Blog create new ticket success.")
        m = re.search(pattern, data)
        if m:
            addresse = self.PROP_URL_POST + \
                "?id=" + str(m.groups()[0]) + "&crea=1"

        return addresse

    #=========================================================================
    # create the corresponding line expected to be used by the blog ticket
    #=========================================================================
    @staticmethod
    def get_blog_line(blog_path, image_name, thumb_img=None):

        filename_tuple = os.path.splitext(image_name)
        string_return = "[(("
        # Ajout de ma propre miniature
        if thumb_img is None:
            string_return += blog_path + "." + \
                filename_tuple[0] + "_m" + filename_tuple[1].lower() + "|"
        else:
            string_return += blog_path + thumb_img + "|"

        string_return += " |"
        string_return += "C|"
        string_return += filename_tuple[0] + "))|"
        string_return += blog_path + image_name + "]"
        string_return += "%%%"
        return string_return

if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG)
    manip = InterractionBlog("http://jingl3s.blog.free.fr/admin",
                             "Canada_2019/Toronto", "Canada_2019", False)
    manip.login_blog("jingl3s", "sobahugo;31")
    # manip.upload_photo(
    #    "/mnt/1T/09_Photos/2019.07.27_Toronto/dossier/2019.07.28_1926_Toronto_m.jpg")
    manip.create_new_ticket(("My text", "second line"), "", "Test")
