'''
Created on 14 nov. 2014

@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''

import os, glob

#===============================================================================
# Generate a dictionary of files name only with each number of PROP_NUMBER_FILES
# The last element of a list is repeated at the init of following list to have the transition
# 
#===============================================================================
def generate_files_list_glob(root_path, wildcard = ""):

    _counter = 0
    _master_counter = 0
    _file_list = []

    if wildcard == "":
        wildcard = "*.JPG"
        

    #Browse the selected folder and subfolders
    files = glob.glob(root_path + os.path.sep + wildcard)
    
    #Browse the selected folder and subfolders for lower case wildcard
    files += glob.glob(root_path + os.path.sep + wildcard.lower())

    files.sort()
    
    # Parse all files found
    for _lFilenameInfos in files:
    
        _file = os.path.basename(_lFilenameInfos)
    
        if _file.lower().endswith(".jpg"):
            _file_list.append(_file)
    
    
    #_file_list.sort()
    return _file_list

    
#===========================================================================
# create the corresponding line expected to be used by the blog ticket
#===========================================================================
def get_blog_line(blog_path, image_name):
    
    filename_tuple = os.path.splitext(image_name)
    string_return = "[(("
    string_return += blog_path + "." + filename_tuple[0] + "_m" + filename_tuple[1].lower() + "|"
    string_return += " |"
    string_return += "C|"
    string_return += filename_tuple[0] + "))|"
    string_return += blog_path + image_name + "]"
    string_return += "%%%"
    return string_return
    