#!/usr/bin/python
# -*- coding: latin-1 -*-
'''
Created on Oct 1, 2014

@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
import os
import sys
#from package import InterractionBlog
from package import interraction_site  as InterractionBlog

from package import ModifyImages


from package.GestionPrefs import GestionPrefs, Fields
from package import Functions
from time import strftime, localtime
import logging
import time

LOGGER = None
_test_mode = False

#=========================================================================
# Manage of images transformations
#=========================================================================


def manage_images_format(image_path):

    if os.path.exists(image_path) == False:
        LOGGER.info("The path is not found.\n" + "Value: " +
                    image_path + "\nPlease check the folder.")
        _liste_fichiers = []
        _liste_fichiers_medium = []

    else:
        LOGGER.info("R�cuperation des images")
        _liste_fichiers = Functions.generate_files_list_glob(image_path)
        _liste_fichiers_medium = []

        LOGGER.info("Modification des images pour le blog")
#         _modif_image = ModifyImages.ModifyImages(image_path)


        # Utilisation forc� de ImageMagick pour un autre resultat de conversion
        _modif_image = ModifyImages.ModifyImages(image_path, mode_pil=False)

        #Tri des image pour sotir les images qui seraient deja en thumb
        _liste_fichiers.sort()
        
        for image_in_progress in _liste_fichiers:
            _modif_image.resize_image(image_in_progress)
            
            position = image_in_progress.rfind(".")

#             img_base_name = os.path.basename(image_in_progress)
            img_base_name = image_in_progress[0:position][-2:]
            
            if not img_base_name == "_m":

                # Creation de l'image thumbnail seulelement si elle n'existe pas deja            
                dest_image_medium = image_in_progress[0:position] + "_m" + image_in_progress[position:len(image_in_progress)]
            
#             if not dest_image_medium in _liste_fichiers and \
#                 not img_base_name in _liste_fichiers_medium:
                _liste_fichiers_medium.append(dest_image_medium)
                _modif_image.copy_resize_image_medium(image_in_progress, dest_image_medium)

        LOGGER.info("Suppression des images sources thumb avec la liste thumbs")

        for image_src in _liste_fichiers:
            if image_src in _liste_fichiers_medium:
                LOGGER.info("Suppression doublons: %s" % image_src)
                # del _liste_fichiers[_liste_fichiers.index(image_src)]
                _liste_fichiers.remove(image_src)

        # Verify si les 2 listes ont la meme taille
        if len(_liste_fichiers) != len(_liste_fichiers_medium): 
            raise RuntimeError("Les listes de fichiers sources et destinations thumb ne sont pas egales\nsrc:%s\nthumb:%s" % (_liste_fichiers, _liste_fichiers_medium))

        _liste_fichiers.sort()
        _liste_fichiers_medium.sort()

    return _liste_fichiers, _liste_fichiers_medium

#=========================================================================
# Manage of blog image preparation
#=========================================================================


def manage_blog(list_picture, preference, p_test_mode, p_blog_ticket_title = None, p_list_thumb = None):
    '''
    
    :param list_picture:
    :param preference:
    :param p_test_mode:
    :param p_blog_ticket_title: optionnel titre du billet si d�sir� sinon utilisation de la concatenation de celui du fichier ini avec un format date
    '''

    path_picture_blog_link = preference[Fields.BLOG_URL_MEDIA_FOR_LINK]\
        + "/"\
        + preference[Fields.BLOG_URL_MEDIA_FOLDER_USED]\
        + "/"

    blog = InterractionBlog.InterractionBlog(preference[Fields.BLOG_URL_ADMIN],
                                             preference[
                                                 Fields.BLOG_URL_MEDIA_FOLDER_USED],
                                             preference[
                                                 Fields.BLOG_POST_CATEGORY],
                                             p_test_mode=p_test_mode)

    login_blog = blog.login_blog(
        preference[Fields.BLOG_LOGIN], preference[Fields.BLOG_PASS])

    if login_blog == True:
        # Login success process blog post with images

        LOGGER.info("Uploading photos in progress")

        # Add return line
        _text = ['%%%', '%%%']
        for idx_img, image_in_progress in enumerate(list_picture):

            # Upload all pictures
            #             print "Upload desactive pour : " + image_in_progress
            blog.upload_photo(
                preference[Fields.SRC_FOLDER] + os.path.sep + image_in_progress)




            # Gestion des thumb manuelle
            thumb_img = None
            if p_list_thumb != None:

#                 position = image_in_progress.rfind(".")

                # Creation de l'image thumbnail seulelement si elle n'existe pas deja            
#                 extract_thumb_chars = image_in_progress[position-2:position]

#                 if not extract_thumb_chars == "_m.":
                
                thumb_img = p_list_thumb[idx_img]
                
                blog.upload_photo(
                    preference[Fields.SRC_FOLDER] + os.path.sep + thumb_img)

            # get link line for blog post
            blog_line = blog.get_blog_line(
                path_picture_blog_link, image_in_progress, thumb_img)
            _text.append(blog_line)

                
        # Add return line
        _text.append('%%%')
        _text.append('%%%')

        LOGGER.info("Blog upload finished")
        if p_blog_ticket_title != None:
            blog_ticket_title = p_blog_ticket_title
        else:
            blog_ticket_title = preference[
                                           Fields.BLOG_POST_TITLE] + " " + strftime("%Y.%m.%d_%H.%M.%S", localtime())
                                           
        addresse = blog.create_new_ticket(
            _text, list_picture[0], blog_ticket_title)
        LOGGER.info("Adresse billet : \n" + addresse)
        if addresse != "":
            os.system('xdg-open ' + addresse + ' &')
        else:
            LOGGER.error("Ouverture du ticket avec le navigateur a �chou�")
        LOGGER.info(
            "Le billet est cr�� avec le lien vers les images. Il est disponible dans l'interface d'administration")

    else:
        with open(preference[Fields.SRC_FOLDER] + os.path.sep + "Blog.txt", 'w') as fileoutput:
            fileoutput.write('\n'.join(map(str, _text)))
            LOGGER.error("La connexion a �chou�.\nLes images sont disponibles pour un envoi manuel.\nLe contenu du billet est disponible : '" +
                         preference[Fields.SRC_FOLDER] + os.path.sep + "Blog.txt'")


def initialize_logger(output_dir, file_basename):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # create console handler and set level to info
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create error file handler and set level to error
    handler = logging.FileHandler(os.path.join(
        output_dir, file_basename + "_" + time.strftime("%Y.%m.%d_%H.%M.%S") + ".log"), "w", encoding=None, delay="true")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def prepare_blog(src_folder=None, blog_folder=None, p_blog_title = None):
    '''

    :param src_folder:
    '''

    path = os.path.abspath(os.path.dirname(__file__))
    filename_python = os.path.basename(__file__)

    LOGGER = initialize_logger(
        path + os.path.sep + "log" + os.path.sep, os.path.splitext(os.path.split(filename_python)[1])[0])
    globals()["LOGGER"] = LOGGER

    gest_pref = GestionPrefs(path, filename_python)
    preference = gest_pref.get_preferences()

    if preference is not None:

        if src_folder != None:
            preference[Fields.SRC_FOLDER] = src_folder

        # Remplace le blog folder par defaut
        if blog_folder != None:
            preference[
                Fields.BLOG_URL_MEDIA_FOLDER_USED] = blog_folder

        _list_image, _list_thumbnail = manage_images_format(preference[Fields.SRC_FOLDER])

        if len(_list_image) > 0:
            manage_blog(_list_image, preference, _test_mode, p_blog_title, p_list_thumb = _list_thumbnail)
#             sys.exit(0)
        else:
            LOGGER.error(
                "Aucune image trouv�e pour envoyer sur le blog dans le dossier '" + preference[Fields.SRC_FOLDER] + "'.")
#             sys.exit(2)
    else:
        LOGGER.error("Aucune pr�f�rence trouv�. Arret des traitements")
#         sys.exit(1)


#=========================================================================
# MAIN
#=========================================================================
if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG)
    prepare_blog()
